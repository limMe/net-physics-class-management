﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddProposal.aspx.cs" Inherits="AddProposal" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@3x.png" sizes="640x1096">
<meta name="apple-mobile-web-app-capable" content="yes"/>
<title>物理本科班级管理系统 SmilingEye</title>
<link href="style/style.css" rel="stylesheet" type="text/css">
<link href="style/color.css" rel="stylesheet" type="text/css">
<link href="style/buttons.css" rel="stylesheet" type="text/css">
<link href="style/retina.css" media="only screen and (-webkit-min-device-pixel-ratio: 2)" rel="stylesheet" />
<link href="style/slider.css" rel="stylesheet" type="text/css">
<link href="style/photoswipe.css" rel="stylesheet" type="text/css">
<link href="style/touchTouch.css" rel="stylesheet" type="text/css">

<script src="scripts/jquery.js"></script>
<script src="scripts/contact.js"></script>
<script src="scripts/swipe.js"></script>
<script src="scripts/klass.min.js"></script>
<script src="scripts/photoswipe.js"></script>
<script src="scripts/touchTouch.js"></script>
<script src="scripts/retina.js"></script>
<script src="scripts/custom.js"></script>
</head>

<body>

    <form id="form1" runat="server">

<div class="header">
	<img src="images/logo.png" alt="img" class="logo replace-2x" width="91" height="25" />
    <a href="#" class="deploy-navigation"></a>
    <a href="#" class="hide-navigation"></a>
	<div class="clear"></div>
</div>

<div class="deployed-navigation">
    <a href="index.aspx" id="ico1" class="navigation-icon home"> 议事大厅 </a>
    <a href="Proposal.aspx" id="ico2" class="navigation-icon speach"> 议事提案 </a>
    <a href="Announcement.aspx" id="ico3" class="navigation-icon alert3"> 通知通告 </a>
    <a href="Platform.aspx" id="ico4" class="navigation-icon cloud-up"> 开放平台</a>
    <a href="Finance.aspx" id="ico5" class="navigation-icon speach2"> 金融顾问 </a>
</div>

<div class="decoration"></div>

<div class="content">

    <div class="container">
    	<h2 class="heading heading-color">提出议案</h2>
        <p class="left-text">
        	大物理班是一个开放的班级，任何人都允许在这里发出提案。它可以用来投票、议论甚至是收费。但由于你的身份不同，某些功能可能会有所限制。
		</p>
        <p class="extended">具体限制包括：只有班委、班代会主席和代表有权提出投票；班代会代表所提出的投票仅能让班代会内部参与投票。</p>
        <!--注意到这个标题栏长度实际上不具有可调整性，以及详细描述的对齐不准确。后期优化可以从这里着手-->
        <h3 class="center-text">议案标题：<asp:TextBox ID="Title" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
        <h3 class="center-text">详细描述：<asp:TextBox ID="Content" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
        <h3 class="center-text">是否投票：<asp:CheckBox ID="IsVote" runat="server" Text="是" OnCheckedChanged="IsVote_CheckedChanged" AutoPostBack="True"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3>
        <asp:Panel ID="LevelPanel" runat="server" Visible="False"><h3 class="center-text">全班投票：<asp:CheckBox ID="Level" runat="server" Text="是" OnCheckedChanged="PLevel_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </h3>
            <div class="notification-box yellow-box">
            <h4>提示</h4>
            <a href="#" class="close-notification-yellow">x</a>
            <div class="clear"></div>
            <p>
                选择全班投票将允许班级内所有成员投票，甚至包括班主任和辅导员。如果不选择是，那么投票将仅允许班代表进行。
            </p>  
        </div>
        </asp:Panel>
        <!--注意到这些panel的展开没有应用符合直觉的js代码动画-->
        <asp:Panel ID="VotePanel" runat="server" Visible="false">
            <h3  class="center-text">开始时间：<asp:TextBox ID="StartDate" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px">01/01/2001 12:00:00</asp:TextBox>
            </h3>
            <h3  class="center-text">截止时间：<asp:TextBox ID="EndDate" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px">12/31/2020 12:00:00</asp:TextBox>
            </h3>
             <h3 class="center-text">投票项一：<asp:TextBox ID="TextBox1" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
             <h3 class="center-text">描述项一：<asp:TextBox ID="TextBox2" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
            <p></p>
            <h3 class="center-text">投票项二：<asp:TextBox ID="TextBox3" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
             <h3 class="center-text">描述项二：<asp:TextBox ID="TextBox4" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
            <p></p>
            <h3 class="center-text">投票项三：<asp:TextBox ID="TextBox5" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
             <h3 class="center-text">描述项三：<asp:TextBox ID="TextBox6" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
            <p></p>
            <h3 class="center-text">投票项四：<asp:TextBox ID="TextBox7" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
             <h3 class="center-text">描述项四：<asp:TextBox ID="TextBox8" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
            <p></p>
            <h3 class="center-text">投票项五：<asp:TextBox ID="TextBox9" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
             <h3 class="center-text">描述项五：<asp:TextBox ID="TextBox10" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
            <p></p>
            <h3 class="center-text">投票项六：<asp:TextBox ID="TextBox11" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
             <h3 class="center-text">描述项六：<asp:TextBox ID="TextBox12" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
            <p></p>
            <h3 class="center-text">投票项七：<asp:TextBox ID="TextBox13" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
             <h3 class="center-text">描述项七：<asp:TextBox ID="TextBox14" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></h3>
            <p></p>
        </asp:Panel>
        <h3 class="center-text">借记收费：<asp:CheckBox ID="IsCost" runat="server" Text="是" OnCheckedChanged="IsCost_CheckedChanged" AutoPostBack="True"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3>
        <asp:Panel ID="FinancePanel" runat="server" Visible="false">
            <h3 class="center-text">收费金额：<asp:TextBox ID="Amount" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large" Width="300px"></asp:TextBox></h3>
            <div class="notification-box yellow-box">
            <h4>提示</h4>
            <a href="#" class="close-notification-yellow">x</a>
            <div class="clear"></div>
            <p>
                借记收费将把本次收费在系统中记录。你可以随时在线下收费并到上线进行清算。对于有收费要求的提案，只有在班代会主席审核认可之后，方能生效。
            </p>  
            </div>
        </asp:Panel>
        <p class="center-text">
        	&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="SubmitBtn" runat="server" Text="提交" BackColor="#00A6E8" BorderStyle="Outset" Font-Italic="False" Font-Names="微软雅黑" Font-Overline="False" Font-Size="Large" OnClick="SubmitBtn_Click" /></p>
        <asp:Panel ID="ErrorPanel" runat="server" Visible="False">
            <div class="notification-box red-box">
            <h4>错误!</h4>
            <a href="#" class="close-notification-red">x</a>
            <div class="clear"></div>
            <p>
                提交议案数据时出现了一个问题。尚不清楚该问题产生的原因，建议你重试一次。对你造成的不便敬请谅解。
            </p>  
        </div>
        </asp:Panel>


    </div>
    <div class="container">  
         <div class="clear">
  
         </div>
     </div>

    <div class="container">  
         <div class="clear"></div>
     </div>
    

</div>

<div class="decoration"></div>

<p class="center-text copyright">Copyright 2012. All rights reserved.</p>


    </form>


</body>
</html>

















