﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// login 的摘要说明
/// </summary>
public partial class login : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UName"] != null)
        {
            Application["UName"] = Request.Cookies["UName"].Value;
            Application["UID"] = Request.Cookies["UID"].Value;
            Application["UPwd"] = Request.Cookies["UPwd"].Value;
            Application["UIsAdmin"] = Request.Cookies["UIsAdmin"].Value;
            Application["UIsSolon"] = Request.Cookies["UIsSolon"].Value;
            Application["UIsChief"] = Request.Cookies["UIsChief"].Value;
            Application["UIsOfficer"] = Request.Cookies["UIsOfficer"].Value;
            Server.Transfer("index.aspx");
        }
    }


    protected void LoginBtn_Click(object sender, EventArgs e)
    {
        string sqlstring = "SELECT * FROM dbo.Users WHERE UID=" + Username.Text + " AND UPwd='" + Password.Text + "'";
        SqlConnection con = new SqlConnection();
        //con.ConnectionString = "data source=210.209.86.225,1433;User ID=a0621160607;pwd=941201;Initial Catalog=a0621160607";
        con.ConnectionString = "Data Source=(local);Initial Catalog=SE;User Id=sa;Password=123456;";
        if (Request.Cookies["UID"] == null)
        {
            if (con.State.Equals(ConnectionState.Closed))
            {
                try
                {
                    con.Open();
                    SqlDataAdapter ad = new SqlDataAdapter();
                    ad.SelectCommand = new SqlCommand(sqlstring, con);
                    con.Close();
                    DataSet ds = new DataSet();
                    ad.Fill(ds, "U");
                    if (ds.Tables.Count == 1 &&ds.Tables[0].Rows.Count==0)
                        throw new Exception();
                    DataTable t1 = ds.Tables["U"];
                    #region 读取相关用户信息，思考怎样增加这部分的代码重用性
                    DataColumn c1 = t1.Columns["UName"];
                    foreach (DataRow row in t1.Rows)
                    {
                        Application["UName"] = row[c1];
                        //eid1 这里用到了foreach来处理这个问题，我觉得应该有更好的方法，毕竟我们只需要读取一个值
                    }
                    c1 = t1.Columns["UPwd"];
                    foreach (DataRow row in t1.Rows)
                        Application["UPwd"] = row[c1];//eid1 
                    c1 = t1.Columns["UID"];
                    foreach (DataRow row in t1.Rows)
                        Application["UID"] = row[c1];//eid1 
                    c1 = t1.Columns["UIsAdmin"];
                    foreach (DataRow row in t1.Rows)
                        Application["UIsAdmin"] = row[c1];//eid1
                    c1 = t1.Columns["UIsSolon"];
                    foreach (DataRow row in t1.Rows)
                        Application["UIsSolon"] = row[c1];//eid1
                    c1 = t1.Columns["UIsChief"];
                    foreach (DataRow row in t1.Rows)
                        Application["UIsChief"] = row[c1];//eid1
                    c1 = t1.Columns["UIsOfficer"];
                    foreach (DataRow row in t1.Rows)
                        Application["UIsOfficer"] = row[c1];//eid1
                    if (AllowCookie.Checked)
                    {
                        Response.Cookies["UName"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UPwd"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UID"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UIsAdmin"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UIsSolon"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UIsChief"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UIsOfficer"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["UIsOfficer"].Value = Application["UIsOfficer"].ToString();
                        Response.Cookies["UIsChief"].Value = Application["UIsChief"].ToString();
                        Response.Cookies["UIsSolon"].Value = Application["UIsSolon"].ToString();
                        Response.Cookies["UIsAdmin"].Value = Application["UIsAdmin"].ToString();
                        Response.Cookies["UID"].Value = Application["UID"].ToString();
                        Response.Cookies["UPwd"].Value = Application["UPwd"].ToString();
                        Response.Cookies["UName"].Value = Application["UName"].ToString();
                    }
                    #endregion
                    Server.Transfer("index.aspx");
                }
                catch (Exception e1)
                {
                    Username.Text = e1.ToString();
                    Panel1.Visible = true;
                }
            }
        }
    }
}