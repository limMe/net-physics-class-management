﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@3x.png" sizes="640x1096">
<meta name="apple-mobile-web-app-capable" content="yes"/>
<title>物理本科班级管理系统 SmilingEyee</title>
<link href="style/style.css" rel="stylesheet" type="text/css">
<link href="style/color.css" rel="stylesheet" type="text/css">
<link href="style/buttons.css" rel="stylesheet" type="text/css">
<link href="style/retina.css" media="only screen and (-webkit-min-device-pixel-ratio: 2)" rel="stylesheet" />
<link href="style/slider.css" rel="stylesheet" type="text/css">
<link href="style/touchTouch.css" rel="stylesheet" type="text/css">

<script src="scripts/jquery.js"></script>
<script src="scripts/contact.js"></script>
<script src="scripts/swipe.js"></script>
<script src="scripts/klass.min.js"></script>
<script src="scripts/photoswipe.js"></script>
<script src="scripts/touchTouch.js"></script>
<script src="scripts/retina.js"></script>
<script src="scripts/custom.js"></script>
</head>

<body>

    <form id="form1" runat="server">

<div class="header">
	<img src="images/logo.png" alt="img" class="logo replace-2x" width="91" height="25"  />
    <a href="#" class="deploy-navigation"></a>
    <a href="#" class="hide-navigation"></a>
	<div class="clear"></div>
</div>

<div class="deployed-navigation">
    <a href="index.aspx" id="ico1" class="navigation-icon home"> 议事大厅 </a>
    <a href="Proposal.aspx" id="ico2" class="navigation-icon speach"> 议事提案 </a>
    <a href="Announcement.aspx" id="ico3" class="navigation-icon alert3"> 通知通告 </a>
    <a href="Platform.aspx" id="ico4" class="navigation-icon cloud-up"> 开放平台</a>
    <a href="Finance.aspx" id="ico5" class="navigation-icon speach2"> 金融顾问 </a>
</div>

<div class="decoration"></div>

    <div class="content">
        <h4>您好，<asp:Label ID="NameLabel" runat="server" Text="黑客"></asp:Label>，欢迎来到在线管理系统议事大厅！
        </h4>
        </div>


    <div class="decoration"></div>
<div class="content">
	<div class="left-box">
    	<h3>议事提案</h3>
        <p>
        	不论是班代会上讨论和投票中的议题，还是班委或者其他人正在进行的报名、统计等事件，都会展现在这个版块。你可以参与投票、报名以及评论。
            <!-- extended 展示的只是在宽屏上才会显示的内容 -->
            <!--<em class="extended">
            	
            </em>-->
        </p>
    </div>
    <div class="right-box">
		<a href="about.html" class="big-metro metro-info">About</a>
        <a href="folio.html" class="big-metro metro-folio">Folio</a>
        <div class="clear"></div>
        <a href="AddProposal.aspx" class="small-metro metro-pencil"></a>
        <a href="about.html" class="small-metro metro-settings"></a>
        <a href="video.html" class="small-metro metro-video margin-fix"></a>
        <a href="folio.html" class="small-metro metro-images margin-fix"></a>
    </div>
    
    <div class="clear"></div>
    <div class="extended"><p>此处将展示最近的议案提案</p></div>   
</div>

<div class="decoration"></div>

<div class="content">
    
	<div class="left-box">
    	<h3>通告通知</h3>
        <p>
			这里提供了一些通知和新闻。你仅仅可以发布通知和浏览通知。任何需要投票或网络结算的事项都不适合发布在这里。
        </p>
    </div>
    
    <div id="slider" class="swipe">
        <ul>
            <li style='display:block'>
                <div>
                    <img class="swipe-img" src="images/slider/1.jpg" alt="img">
                    <p class="swipe-text">Swipe on device!</p>
                </div>
            </li>
            <li style='display:none'>
                <div>
                    <img class="swipe-img" src="images/slider/2.jpg" alt="img">
                    <p class="swipe-text">Please! Swipe me!</p>
                </div>
            </li>
            <li style='display:none'>
                <div>
                    <img class="swipe-img" src="images/slider/3.jpg" alt="img">
                    <p class="swipe-text">Works on mobiles!</p>
                </div>
            </li>
            <li style='display:none'>
                <div>
                    <img class="swipe-img" src="images/slider/4.jpg" alt="img">
                    <p class="swipe-text">Adorably draggable!</p>
                </div>
            </li>
        </ul>
    </div>
    <div class="clear"></div>  
    <div><p>此处将展示最近的通知</p></div>   
</div>

<div class="decoration"></div>

<div class="content">
	<div class="left-box">
    	<h3>开放平台</h3>
        <p>
			开放平台提供了一系列接口允许任何本班程序爱好者编写运行在SmilingEye平台上的网页应用。普通用户也可以在此享受到丰富的在线应用哦！
        </p>
    </div>
    <div class="right-box">
    	<div class="line-one">
            <a href="tel:+12 345 6789" class="medium-metro-bottom metro-phone"></a>
            <a href="sms:+12 345 6789" class="medium-metro-bottom metro-text"></a>
        </div>
        <div class="line-two">
    		<a href="#" class="big-metro-bottom metro-mail">CONNECT</a>
        </div>
        <div class="line-three">
        	<a href="http://www.facebook.com/enabled.labs" class="small-metro-bottom metro-facebook"></a>
            <a href="https://twitter.com/iEnabled" class="small-metro-bottom metro-twitter"></a>
            <a href="http://dribbble.com/enabled" class="small-metro-bottom metro-dribbble"></a>
            <a href="http://db.tt/LdzQJFR" class="small-metro-bottom metro-dropbox"></a>
        </div>
    </div>
	<div class="clear"></div>
     <div class="container">  
         <div class="clear">
           
         </div>
     </div>
</div>



<div class="decoration"></div>

<p class="center-text copyright">Copyright 2013 by MYAI. All rights reserved.</p>


    </form>


</body>
</html>

















