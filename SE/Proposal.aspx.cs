﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

/// <summary>
/// Proposal 的摘要说明
/// </summary>
public partial class Proposal : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Application["UName"] == null)
            Server.Transfer("login.aspx");
        //string sqlstring = "SELECT top 10 PSponsorName,PTitle,PID FROM Proposal ORDER BY PPostTime DESC";
        string sqlstring = "SELECT top 10 PSponsorName,PTitle,PID FROM dbo.Proposal ORDER BY PPostTime DESC";
        SqlConnection con = new SqlConnection();
        con.ConnectionString = "Data Source=(local);Initial Catalog=SE;User Id=sa;Password=123456;";
        //con.ConnectionString = "Data Source=210.209.86.225,1433;Initial Catalog=a0621160607;User Id=a0621160607;Password=941201";
        if (con.State.Equals(ConnectionState.Closed))
        {
            try
            {
                con.Open();
                SqlCommand ReadProposal = new SqlCommand(sqlstring, con);
                SqlDataReader reader = ReadProposal.ExecuteReader();
                while (reader.Read())
                {
                    
                    HyperLink AProposal = new HyperLink();
                    AProposal.Text = reader["PSponsorName"].ToString() + "提议说：“" + reader["PTitle"].ToString() + "”";
                    AProposal.NavigateUrl = "ProposalDtl.aspx?id=" + reader["PID"].ToString();
                    Panel1.Controls.Add(AProposal);
                    Panel1.Controls.Add(new System.Web.UI.LiteralControl("<BR/>")); 
                }
            }
            catch (Exception)
            {
                
            }
            finally
            {
                con.Close();
            }
        }

    }

}