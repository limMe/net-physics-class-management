﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// AddProposal 的摘要说明
/// </summary>
public partial class AddProposal : System.Web.UI.Page
{
    string PIsVote,PIsCost,PLevel;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Application["UName"] == null)
            Server.Transfer("login.aspx");
        PIsVote = "0"; PIsCost = "0"; PLevel = "0";

    }

    protected void IsVote_CheckedChanged(object sender, EventArgs e)
    {
        if (IsVote.Checked==true)
        {
            PIsVote = "1";
            VotePanel.Visible = true;
            if (Application["UIsChief"] == "1" || Application["UIsOfficer"] == "1")
                LevelPanel.Visible = true;
        }
        else
        {        
            PIsVote = "0";
            VotePanel.Visible = false;
            LevelPanel.Visible = false;
            PLevel = "0";
        }

    }
    protected void IsCost_CheckedChanged(object sender, EventArgs e)
    {
        if (IsCost.Checked==true)
        {
            PIsCost = "1";
            FinancePanel.Visible = true;
        }
        else
        {
            PIsCost = "0";
            FinancePanel.Visible = false;
        }
    }

    protected void SubmitBtn_Click(object sender, EventArgs e)
    {
        String INSERT_SQL =
    @"
INSERT INTO [Proposal] (
  PTitle, PContent, PSponsorID,PSponsorName,PLevel,PIsVote,PIsCost,PPostTime,PStartTime,PEndTime
) VALUES (
 @PTitle, @PContent, @PSponsorID,@PSponsorName,@PLevel,@PIsVote,@PIsCost,@PPostTime,@PStartTime,@PEndTime)";
        //string sqlstring = "SELECT * FROM dbo.Users WHERE UID=" + Username.Text + " AND UPwd='" + Password.Text + "'";
        SqlConnection con = new SqlConnection();
        //con.ConnectionString = "Data Source=210.209.86.225,1433;Initial Catalog=a0621160607;User Id=a0621160607;Password=941201";
        con.ConnectionString = "Data Source=(local);Initial Catalog=SE;User Id=sa;Password=123456;";
        if (con.State.Equals(ConnectionState.Closed))
        {
            try
            {
                con.Open();
                SqlCommand AddProposalCom = con.CreateCommand();
                AddProposalCom.CommandText = INSERT_SQL;
                //AddProposalCom.Parameters.Add(new SqlParameter("@PID","3"));
                AddProposalCom.Parameters.Add(new SqlParameter("@PTitle",Title.Text));
                AddProposalCom.Parameters.Add(new SqlParameter("@PContent",Content.Text));
                AddProposalCom.Parameters.Add(new SqlParameter("@PSponsorID", Application["UID"]));
                AddProposalCom.Parameters.Add(new SqlParameter("@PSponsorName", Application["UName"]));
                AddProposalCom.Parameters.Add(new SqlParameter("@PLevel", PLevel));
                AddProposalCom.Parameters.Add(new SqlParameter("@PIsVote",PIsVote));
                AddProposalCom.Parameters.Add(new SqlParameter("@PIsCost",PIsCost));
                AddProposalCom.Parameters.Add(new SqlParameter("@PPostTime",DateTime.Now.ToString()));
                AddProposalCom.Parameters.Add(new SqlParameter("@PStartTime",StartDate.Text));
                AddProposalCom.Parameters.Add(new SqlParameter("@PEndTime", EndDate.Text));
                //AddProposalCom.Parameters.Add(new SqlParameter("@PPostTime", DateTime.Now.ToString()));
                AddProposalCom.ExecuteNonQuery();
            }
            catch(Exception e1)
            {
                //Title.Text = e1.ToString();
                ErrorPanel.Visible = true;
            }
            finally
            {
                con.Close();
                Server.Transfer("index.aspx");
            }
        }
    }

    protected void PLevel_CheckedChanged(object sender, EventArgs e)
    {
        if (Level.Checked==true)
            PLevel = "1";
        else
            PLevel = "0";
    }
}