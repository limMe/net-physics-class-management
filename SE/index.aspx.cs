﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// index 的摘要说明
/// </summary>
public partial class index : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Application["UName"] == null)
            Server.Transfer("login.aspx");
        else
        {
            NameLabel.Text = Application["UName"].ToString();
        }
    }

    
}