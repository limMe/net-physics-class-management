﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/splash-icon.png"> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@3x.png" sizes="640x1096">
<meta name="apple-mobile-web-app-capable" content="yes"/>
<title>用户登陆 SmilingEye</title>
<link href="style/style.css" rel="stylesheet" type="text/css">
<link href="style/color.css" rel="stylesheet" type="text/css">
<link href="style/buttons.css" rel="stylesheet" type="text/css">
<link href="style/retina.css" media="only screen and (-webkit-min-device-pixel-ratio: 2)" rel="stylesheet" />
<link href="style/slider.css" rel="stylesheet" type="text/css">
<link href="style/photoswipe.css" rel="stylesheet" type="text/css">
<link href="style/touchTouch.css" rel="stylesheet" type="text/css">

<script src="scripts/jquery.js"></script>
<script src="scripts/contact.js"></script>
<script src="scripts/swipe.js"></script>
<script src="scripts/klass.min.js"></script>
<script src="scripts/photoswipe.js"></script>
<script src="scripts/touchTouch.js"></script>
<script src="scripts/retina.js"></script>
<script src="scripts/custom.js"></script>
</head>

<body>

    <form id="form1" runat="server">

<div class="header">
	<img src="images/logo.png" alt="img" class="logo replace-2x" width="91" height="25" />
    <a href="#" class="deploy-navigation"></a>
    <a href="#" class="hide-navigation"></a>
	<div class="clear"></div>
</div>

<div class="deployed-navigation">
    <a href="index.aspx" id="ico1" class="navigation-icon home"> 议事大厅 </a>
    <a href="Proposal.aspx" id="ico2" class="navigation-icon speach"> 议事提案 </a>
    <a href="Announcement.aspx" id="ico3" class="navigation-icon alert3"> 通知通告 </a>
    <a href="Platform.aspx" id="ico4" class="navigation-icon cloud-up"> 开放平台</a>
    <a href="Finance.aspx" id="ico5" class="navigation-icon speach2"> 金融顾问 </a>
</div>

<div class="decoration"></div>

<div class="content">

    <div class="container">
    	<h2 class="heading heading-color">登录系统</h2> 
        <h3 class="center-text">
        	学号：<asp:TextBox ID="Username" runat="server" BackColor="#0099CC" BorderStyle="None" Font-Size="Large">2013201384</asp:TextBox>
		</h3>
        <p class="center-text">
        	&nbsp;</p>
        <h3 class="center-text">
        	密码：<asp:TextBox ID="Password" runat="server" TextMode="Password" BackColor="#0099CC" Font-Size="Large">941201</asp:TextBox>
		</h3>
        <p class="center-text">
		    &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="AllowCookie" runat="server" Text="记住我" />
		</p>
        <p class="center-text">
        	&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="LoginBtn" runat="server" Text="登录" BackColor="#00A6E8" BorderStyle="Outset" Font-Italic="False" Font-Names="微软雅黑" Font-Overline="False" Font-Size="Large" OnClick="LoginBtn_Click" /></p>
        <p class="center-text">
        	</p>
        <asp:Panel ID="Panel1" runat="server" Visible="False">
            <div class="notification-box red-box">
            <h4>注意!</h4>
            <a href="#" class="close-notification-red">x</a>
            <div class="clear"></div>
            <p>
                你的用户名或者密码出现了错误。如果你确定你的输入是正确的，那么你可能存在盗号风险，这种情况请联系钟典。
            </p>  
        </div>
        </asp:Panel>
    <div class="container">  
         <div class="clear">

         </div>
     </div>

    <div class="container">  
         <div class="clear"></div>
     </div>
    

</div>

<div class="decoration"></div>

<p class="center-text copyright">Copyright 2012. All rights reserved.</p>


    </form>


</body>
</html>

















