﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProposalDtl.aspx.cs" Inherits="ProposalDtl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
<link rel="apple-touch-icon" sizes="114x114" href="images/splash/splash-icon.png" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen.png" media="screen and (max-device-width: 320px)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@2x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" /> 
<link rel="apple-touch-startup-image" href="images/splash/splash-screen@3x.png" sizes="640x1096" />
<meta name="apple-mobile-web-app-capable" content="yes"/>
<title>物理本科班级管理系统 SmilingEye</title>
<link href="style/style.css" rel="stylesheet" type="text/css" />
<link href="style/color.css" rel="stylesheet" type="text/css" />
<link href="style/buttons.css" rel="stylesheet" type="text/css" />
<link href="style/retina.css" media="only screen and (-webkit-min-device-pixel-ratio: 2)" rel="stylesheet" />
<link href="style/slider.css" rel="stylesheet" type="text/css" />
<link href="style/photoswipe.css" rel="stylesheet" type="text/css" />
<link href="style/touchTouch.css" rel="stylesheet" type="text/css" />

<script src="scripts/jquery.js"></script>
<script src="scripts/contact.js"></script>
<script src="scripts/swipe.js"></script>
<script src="scripts/klass.min.js"></script>
<script src="scripts/photoswipe.js"></script>
<script src="scripts/touchTouch.js"></script>
<script src="scripts/retina.js"></script>
<script src="scripts/custom.js"></script>
</head>

<body>

    <form id="form1" runat="server">

<div class="header">
	<img src="images/logo.png" alt="img" class="logo replace-2x" width="91" height="25" />
    <a href="#" class="deploy-navigation"></a>
    <a href="#" class="hide-navigation"></a>
	<div class="clear"></div>
</div>

<div class="deployed-navigation">
    <a href="index.aspx" id="ico1" class="navigation-icon home"> 议事大厅 </a>
    <a href="Proposal.aspx" id="ico2" class="navigation-icon speach"> 议事提案 </a>
    <a href="Announcement.aspx" id="ico3" class="navigation-icon alert3"> 通知通告 </a>
    <a href="Platform.aspx" id="ico4" class="navigation-icon cloud-up"> 开放平台</a>
    <a href="Finance.aspx" id="ico5" class="navigation-icon speach2"> 金融顾问 </a>
</div>

<div class="decoration"></div>

<div class="content">

    <div class="container">
    	<h2 class="heading heading-color">
            <asp:Label ID="Title" runat="server" Text="Label"></asp:Label>
        </h2>
        <h3 class="left-text">
        	发起人：<asp:Label ID="SponsorName" runat="server" Text="SponsorName"></asp:Label>
		</h3>
        <h3 class="left-text">
        	演讲稿：</h3>
        <h3 class="center-text">
            <asp:Label ID="Content" runat="server" ForeColor="Gray" Text="Label"></asp:Label>
        </h3>
    </div>
    <div class="container">  
         <div class="clear">
  <div class="decoration"></div>
         </div>
     </div>

    <div class="container">  
         <div class="clear"></div>
     </div>
    <div class="container">
    	<h2 class="heading heading-color">投票详情</h2>
        <h3 class="left-text">
        	<asp:Label ID="Notice" runat="server" Text="这个提案不涉及到投票"></asp:Label>
		</h3>
        <asp:Panel ID="VotePanel" runat="server">
        </asp:Panel>
        <h3>以下备注框可以填写发起者需要的相关信息，没有请留空：</h3>
        <p class="center-text">
            <asp:TextBox ID="WriteDtl" runat="server" BackColor="#0099CC" Font-Size="Large" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox>
        </p>
        <p class="center-text">
            &nbsp;</p>
        <p class="center-text">
            <asp:Button ID="Vote" runat="server" Text="提交" BackColor="#00A6E8" BorderStyle="Outset" Font-Italic="False" Font-Names="微软雅黑" Font-Overline="False" Font-Size="Large" OnClick="LoginBtn_Click" />
        </p>
        
    </div>
    <div class="container">  
         <div class="clear"></div>
        <div class="decoration"></div>
     </div>
    <div class="container">
    	<h2 class="heading heading-color">同学评论</h2>
        <h3 class="left-text">
        	以下是近期同学们关于该议题的热议：
		</h3>
        <h3 class="left-text"><asp:Panel ID="CommentPanel" runat="server">
        </asp:Panel></h3>
    </div>

    <div class="container">  
         <div class="clear"></div>
        <div class="decoration"></div>
     </div>
    <div class="container">
    	<h2 class="heading heading-color">添加评论</h2>
        <h3 class="left-text">
        	你可以在这里写下你的想法：
		</h3>
        <h3 class="center-text">
            <asp:TextBox ID="WriteComment" runat="server" BackColor="#0099CC" Font-Size="Large" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox>
        </h3>
        <p class="center-text"><asp:Button ID="SubmitBtn" runat="server" Text="提交" BackColor="#00A6E8" BorderStyle="Outset" Font-Italic="False" Font-Names="微软雅黑" Font-Overline="False" Font-Size="Large" OnClick="LoginBtn_Click" /></p>
    </div>

</div>

<div class="decoration"></div>

<p class="center-text copyright">Copyright 2012. All rights reserved.</p>


    </form>


</body>
</html>

















